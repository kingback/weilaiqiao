/**
 * 迷你登录框
 * @author ningzbruc@gmail.com
 */

YUI.add('mini-login', function(Y) {
    
    var HEADER_CONTENT = '<h2><strong>{headerText}</strong><a href="#" class="login-close">关闭</a></h2>',
        FOOTER_CONTENT = '<a href="#" class="switch-type">{footerText}</a>',
        BODY_CONTENT = '<form action="#" method="get">' + 
                           '<ul>' + 
                               '<li><label class="clearfix"><span>账号：</span><input type="text" /></label></li>' + 
                               '<li><label class="clearfix"><span>密码：</span><input type="password" /></label></li>' + 
                           '</ul>' + 
                           '<p><button type="submit">登录</button></p>' + 
                           '<div class="clearfix">' + 
                               '<a href="#" class="register">注册</a>' + 
                               '<a href="#" class="forget-password">忘记密码？</a>' + 
                           '</div>' +
                       '</form>';
        
    var MiniLogin = Y.Base.create('mini-login', Y.Base, [], {
        
        initializer: function() {
            var type = this.get('type');
            this.rendered = false;
            this.panel = new Y.Panel({
                headerContent: Y.Lang.sub(HEADER_CONTENT, { headerText: type ? '企业登录' : '登录' }),
                bodyContent: BODY_CONTENT,
                footerContent: Y.Lang.sub(FOOTER_CONTENT, { footerText: type ? '个人版登录' : '企业版登录' }),
                width: 510,
                zIndex: 20000,
                centered: true,
                modal: true,
                visible: false,
                hideOn: [{
                    eventName: 'clickoutside'
                }]
            });
        },
        
        destructor: function() {
            this.panel.destroy();
        },
        
        render: function() {
            var self = this,
                type = this.get('type'),
                bb;
            
            this.panel.render();
            
            bb = this.panel.get('boundingBox');
            bb.addClass('login-panel');
            type && bb.addClass('login-enterprise');
            
            bb.one('.yui3-widget-hd .login-close').on('click', function(e) {
                e.preventDefault();
                this.hide(); 
            }, this);
            
            bb.one('.yui3-widget-ft .switch-type').on('click', function(e) {
                e.preventDefault();
                this.switchType(); 
            }, this);
            
            this.panel.after('typeChange', this._afterTypeChange, this);
            
            this.rendered = true;
            
            return this;
        },
        
        show: function() {
            this.panel.show();
            return this;
        },
        
        hide: function() {
            this.panel.hide();
            return this;
        },
        
        close: function() {
            this.destroy();
        },
        
        switchType: function(type) {
            this.panel.set('type', typeof type === 'undefined' ? !this.panel.get('type') : type);
            return this;
        },
        
        _afterTypeChange: function(e) {
            if (!this.rendered) { return; }
            
            var bb = this.panel.get('boundingBox'),
                type = e.newVal,
                headerText = bb.one('.yui3-widget-hd strong'),
                footerText = bb.one('.yui3-widget-ft .switch-type');
                
            headerText.set('innerHTML', type ? '企业登录' : '登录');
            footerText.set('innerHTML', type ? '个人版登录' : '企业版登录');
            bb.toggleClass('login-enterprise', type);
        }
        
    }, {
        
        ATTRS: {
            
            type: {
                value: 0,
                setter: function(v) {
                    return v ? 1 : 0;
                }
            },
            
            redirectUrl: {
                value: location.href
            }
            
        },
        
        TYPE: {
            PERSONAL: 0,
            ENTERPRISE: 1
        }
        
    });
    
    Y.MiniLogin = MiniLogin;
    
}, '0.0.1', {
    requires: ['node', 'event', 'base', 'panel', 'template']
});
