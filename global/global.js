/**
 * 通用逻辑
 */

YUI.add('wlq-global', function(Y) {
    
    var loginBtn = Y.one('.nav-info .login'),
        loginPanel;
    
    if (loginBtn) {
        loginBtn.on('click', function(e) {
            e.preventDefault();
            if (!loginPanel || loginPanel.get('destroyed')) {
                window.WLQMiniLogin = loginPanel = new Y.MiniLogin();
                loginPanel.render();
            }
            loginPanel.show();
        });
    }
    
}, '0.0.1', {
    requires: ['mini-login']
});
