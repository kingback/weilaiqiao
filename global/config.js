/**
 * YUI Config
 */

YUI.GlobalConfig = {
    base: '../../yui/',
    root: '',
    combine: false,
    filter: 'min',
    groups: {
        'global': {
            base: '../../global/',
            root: '',
            combine: false,
            modules: {
                'mini-login': {
                    path: 'mini-login/mini-login.js',
                    requires: ['node', 'event', 'base', 'panel', 'template']
                },
                'wlq-global': {
                    path: 'global.js',
                    requires: ['mini-login']
                }
            }
        }
    }
};

window.WLQ = new YUI();
